export class DestinoViaje {
    private selected: boolean;
    servicios: string[];
    constructor(public nombre: string, public url: string){
        this.servicios = ['pileta','desayuno'];
    }
    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
}
